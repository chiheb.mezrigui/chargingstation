import {StyleSheet} from 'react-native';


//fichier css de composant StationForm

export const SecondStationFormCss = StyleSheet.create({

    subTitle: {
        color: 'black',
        fontStyle: 'normal',
        fontWeight: 'bold',
        fontSize: 27,
        marginBottom: 40
    },
    checkboxStyle:{
        marginLeft:150,
        marginTop:4

    },
    viewStyle: {
        alignSelf: "center",
        flexDirection: "row",
        width: "100%",
        alignItems: "center"
    },
    container: {
        paddingLeft: 30
    },
    addressBorder: {
        width: 330,
        height: 95,
        borderRadius: 10,
        borderWidth: 2,
        borderColor: '#E6E6E6',
        marginBottom: 35,
    },
    timeBorder: {
        width: 330,
        height: 60,
        borderRadius: 10,
        borderWidth: 2,
        borderColor: '#E6E6E6',
        marginBottom: 35,
    },
    item: {
        color: '#8F8F8F',
        fontSize: 20,
        fontWeight: '600',
        marginBottom: 20,
    },
    circleStyle: {
        marginTop: 12,
        marginLeft: 17,
        width: 65,
        height: 65,
        borderRadius: 50,
        borderWidth: 2,
        borderColor: '#F2F2F2',
        backgroundColor: '#F2F2F2'
    },
    iconStyle: {
        color: '#235ef5',
        marginLeft: 15,
        marginTop: 15
    },
    addressText: {
        color: 'black',
        fontWeight: 'bold',
        fontSize: 18,
        marginTop: -60,
        marginLeft: 100,
    },
    secondAddressText: {
        color: '#8F8F8F',
        fontWeight: 'bold',
        fontSize: 18,
        marginLeft: 110,
        marginTop: 5,
    },
    timeAvailable: {
        color: 'black',
        fontWeight: '800',
        fontSize: 20,
        marginTop: 14,
        marginLeft: 20,
        marginBottom:10
    },
   
    button: {
        width: 330,
        padding: 10,
        backgroundColor: '#235ef5',
        borderWidth: 0.5,
        borderRadius: 20,
        borderColor: '#D4D4D4',
        marginTop: 30

    },
    text: {
        color: '#fff',
        fontSize: 18,
        fontWeight: 'bold',
        textAlign: 'center'

    },
})