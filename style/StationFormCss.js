
import {StyleSheet} from 'react-native';

//fichier css de composant SecondStationForm
export const StationFormCss= StyleSheet.create({

    viewStyle: {
        alignSelf: "center",
        flexDirection: "row",
        width: "100%",
        alignItems: "center"
    },
    itemStyle: {
        fontSize: 10,
        color: "#007aff",
        fontWeight:'bold'
    },
    pickerStyle: {
        width: "100%",
        height: 40,
        color: "#007aff",
        fontSize: 14,

    },
    selectedStyle:{
        color: '#A9A9A9',
         marginLeft:-130,
         fontStyle: 'italic'  
    },

    container: {
        paddingLeft: 30
    },


    horizontalLine: {
        marginLeft: 15,
        width: 300,
        borderBottomColor: '#F2F2F2',
        borderBottomWidth: 2

    },
    
  
    selectBorder: {
        width: 330,
        borderWidth: 2, borderRadius: 20, borderColor: '#F2F2F2'
    },


    item: {
        color: '#A9A9A9',
        fontSize: 18,
        fontWeight: '600',
        marginBottom: 17,
    },
    item1: {
        color: '#A9A9A9',
        fontSize: 18,
        fontWeight: '600',
        marginBottom: 25,
    },
    subTitle: {
        color: 'black',
        fontStyle: 'normal',
        fontWeight: 'bold',
        fontSize: 28,
        marginBottom: 40,
    },
    input: {
        backgroundColor: '#F2F2F2',
        width: 330,
        height: 40,
        borderRadius: 25,
        borderColor: 'grey',
        fontSize: 17,
        color: '#000',
        paddingLeft: 20,
        fontFamily: 'Roboto',
        fontWeight: 'bold',
        marginBottom:25

    },
    select: {
        marginLeft: 15,
        width: 300,
        color: "dark",
        backgroundColor: '#ffff',
    },
    button: {
        width: 330,
        padding: 10,
        backgroundColor: '#235ef5',
        borderWidth: 0.5,
        borderRadius: 20,
        borderColor: '#D4D4D4',
        marginTop: 30

    },
    textButton: {
        color: '#fff',
        fontSize: 18,
        fontWeight: 'bold',
        textAlign: 'center'

    },
})