import {StyleSheet} from 'react-native';


//fichier css de composant App

export const AppStyle = StyleSheet.create({

    container: {
        flex: 1,
        backgroundColor: 'white',
        marginTop:50
      },
})