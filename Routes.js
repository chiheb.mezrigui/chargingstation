import React from 'react'
import { Router, Scene } from 'react-native-router-flux'
import StationForm from './components/StationForm.js'
import SecondStationForm from './components/SecondStationForm.js'



const Routes = () => (
    <Router>
       <Scene key = "root">
          <Scene key = "form" component = {StationForm} title = "form" initial = {true} />
          <Scene key = "secondForm" component = {SecondStationForm} title = "form1" />
       </Scene>
    </Router>
 )
 export default Routes