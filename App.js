import * as React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import StationForm from './components/StationForm';
import SecondStationForm from './components/SecondStationForm';
import { AppStyle } from './style/AppStyle';


export default class App extends React.Component {

  render(){
    return (
      <View style={AppStyle.container}>
             {/* <StationForm/>   */}
           <SecondStationForm/>  
      </View>
    );
  }
 
}




