import React from "react";
import { Text, View, SafeAreaView, StyleSheet, TextInput, TouchableOpacity,Alert } from 'react-native';
import { Picker } from "@react-native-picker/picker"; //Bibliothèque Picker
import Topbar from "./Topbar"; //importation du composant TopBar 
import  {StationFormCss} from '../style/StationFormCss' //importation de fichier style de cette composant


export default class StationForm extends React.Component {

    constructor(props) {
        super(props);  //référence au constructeur de la classe parente(React.Component)

        //on utilise (this) qu'aprés avoir appelé le constructeur parent

        this.state = {

            // les variables sont initialement null
            stationName:'',
            selectedEnergyValue:'',
            selectedParkingValue:'',
            selectedAccessValue:'',
            selectedOwnerValue:'',

            //select item
            energySource: [
                {
                    itemName: 'Energy source'  //sous item 
                },
                
                {
                    itemName: 'Renewable'  //sous item 
                },
                {
                    itemName: 'Nonrenewable'  //sous item 
                }
            ],
            serviceType: [
                {
                    itemName:'Service type' //sous item
                },
                {
                    itemName:'' //sous item 
                },
                {
                    itemName: 'Parking' //sous item 
                },
                {
                    itemName: 'Shop' //sous item 
                },
                {
                    itemName: 'Something else' //sous item 
                },
            ],
            access: [
                { 
                    itemName:'Access' //sous item 
                },
                {
                    itemName:'' //sous item 
                },
                {
                    itemName: 'Public' //sous item 
                },
                {
                    itemName: 'Private' //sous item 
                },
                { 
                    itemName: 'Unknown' //sous item 
                },
            ],
            status: [
                {
                    itemName:'Status' //sous item 
                },
                {
                    itemName:'' //sous item 
                },
                {
                    itemName: 'Working' //sous item 
                },
                {
                    itemName: 'Not Working' //sous item 
                }
            ],
            parking: [
                {
                    itemName:'Parking' //sous item 
                },
                {
                    itemName:'' //sous item 
                },
                {
                    itemName: 'Free' //sous item 
                },
                {
                    itemName: 'Paying' //sous item 
                },
                {
                    itemName: 'Unknown' //sous item 
                }
            ],
            owner: [
                {
                    itemName:'Owner' //sous item 
                },
                {
                    itemName:'' //sous item 
                },
                {
                    itemName: 'Working' //sous item 
                },
                {
                    itemName: 'Not Working' //sous item 
                }
            ]
        };
    }
    
  // fonction du boutton (Next) fonction d'affirmation des valeurs entrés  
    handlePress() {
        if (!this.state.stationName.trim()) {  //condition si l'input est vide s'affiche une alert que vous l'obligé d'entrer (station name)
            Alert.alert(
                "Invalid form", // alert title
                "Please enter Station name", // alert text
            );
                }
        else {   // sinon l'affirmation des valeurs entrés
            console.log('Station information :')
            console.log('Station Name: '+ this.state.stationName)
            console.log('Energy Source: '+ this.state.selectedEnergyValue)
            console.log('Parking: '+ this.state.selectedParkingValue)
            console.log('Service Type: '+ this.state.selectedServiceValue)
            console.log('Access: '+ this.state.selectedAccessValue)
            console.log('Owner: '+ this.state.selectedOwnerValue)


        }
    }

    //Async (Asynchrone) signifie que les choses peuvent se produire indépendamment de programme pricipale
    async onValueChangeCat(value) {
        //this.setState : accéder au state ou état de la variable que déja déclarer dans le constructeur  
        this.setState({ selectedEnergyValue: value });

    }
    async onValueChangeParking(value) {
                //this.setState : accéder au state ou état de la variable que déja déclarer dans le constructeur  
        this.setState({ selectedParkingValue: value });
    }
    async onValueChangeServicetype(value) {
                //this.setState : accéder au state ou état de la variable que déja déclarer dans le constructeur  

        this.setState({ selectedServiceValue: value });

    }
    async onValueChangeAccess(value) {
                //this.setState : accéder au state ou état de la variable que déja déclarer dans le constructeur  

        this.setState({ selectedAccessValue: value });
    }
    async onValueChangeOwner(value) {
                //this.setState : accéder au state ou état de la variable que déja déclarer dans le constructeur  

        this.setState({ selectedOwnerValue: value });

    }
   

    render() {

        return (


            <View>
                <Topbar/>  {/* Affichage de composant (TopBar) que déja crée aprés leur importation dans la partie import */}
                <View style={StationFormCss.container}>
                    <Text style={StationFormCss.subTitle}>Station basic info</Text>
                    <View style={{ marginBottom: 40 }}>
                        <Text style={StationFormCss.item}>Station Name</Text>

                        {/* onChangeText: event pour lire l'entrée de l'utilisateur  */}
                        <TextInput
                            style={StationFormCss.input} placeholderTextColor={'#0000'}
                            onChangeText={(stationName) => this.setState({ stationName })} 
                            value={this.state.stationName}  
                        />
                    </View>
                    <View style={{ marginTop: -20 }}>

                        <Text style={StationFormCss.item1}>Other info</Text>
                        <View style={StationFormCss.selectBorder}>

                            <View style={StationFormCss.viewStyle}>
                            {/* onValuechange : event détecté lorsque la valeur de picker est changée */}
                            {/* on applique la fonction ou la methode (bind) pour accéder au variable  */}

                                <Picker itemStyle={StationFormCss.itemStyle} mode="dropdown" prompt="Energy Source" style={StationFormCss.select}
                                    onValueChange={this.onValueChangeCat.bind(this)}>  
                                    {this.state.energySource.map((item) => (
                                        <Picker.Item 
                                            color="black"
                                            label={item.itemName}
                                            value={item.itemName}
                                        />
                                    ))}

                                </Picker>
                                <Text style={StationFormCss.selectedStyle}>{this.state.selectedEnergyValue}</Text>
                            </View>

                            
                            <View style={StationFormCss.horizontalLine} />

                            <View style={StationFormCss.viewStyle}>
                                <Picker itemStyle={styles.itemStyle} mode='dropdown' prompt="Service Type" style={StationFormCss.select}
                                    onValueChange={this.onValueChangeServicetype.bind(this)}>

                                    {this.state.serviceType.map((item) => (
                                        <Picker.Item
                                            color="black"
                                            label={item.itemName}
                                            value={item.itemName}
                                        />
                                    ))}

                                </Picker>
                                <Text style={StationFormCss.selectedStyle}>{this.state.selectedServiceValue}</Text>
                            </View>
                            <View style={StationFormCss.horizontalLine} />
                            <View style={StationFormCss.viewStyle}>
                                <Picker itemStyle={StationFormCss.itemStyle} mode="dropdown" prompt="Parking" style={StationFormCss.select}
                                    onValueChange={this.onValueChangeParking.bind(this)}>

                                    {this.state.parking.map((item) => (
                                        <Picker.Item
                                            color="black"
                                            label={item.itemName}
                                            value={item.itemName}
                                        />
                                    ))}

                                </Picker>
                                <Text style={StationFormCss.selectedStyle}>{this.state.selectedParkingValue}</Text>
                            </View>
                            <View style={StationFormCss.horizontalLine} />
                            <View style={StationFormCss.viewStyle}>
                                <Picker itemStyle={StationFormCss.itemStyle} mode="dropdown" prompt="Access" style={StationFormCss.select}
                                    onValueChange={this.onValueChangeAccess.bind(this)}>

                                    {this.state.access.map((item) => (
                                        <Picker.Item
                                            color="black"
                                            label={item.itemName}
                                            value={item.itemName}
                                        />
                                    ))}

                                </Picker>
                                <Text style={StationFormCss.selectedStyle}>{this.state.selectedAccessValue}</Text>
                            </View>
                            <View style={StationFormCss.horizontalLine} />
                            <View style={StationFormCss.viewStyle}>
                                <Picker itemStyle={StationFormCss.itemStyle} mode="dropdown" prompt="Owner" style={StationFormCss.select}
                                    onValueChange={this.onValueChangeOwner.bind(this)}>

                                    {this.state.owner.map((item) => (
                                        <Picker.Item
                                            color="black"
                                            label={item.itemName}
                                            value={item.itemName}
                                        />
                                    ))}

                                </Picker>
                                <Text style={StationFormCss.selectedStyle}>{this.state.selectedOwnerValue}</Text>
                            </View>
                            
                        </View>

                    </View>

                    <TouchableOpacity
                        style={StationFormCss.button}
                        onPress={() => this.handlePress()}>
                        <Text style={StationFormCss.textButton}>Next</Text>
                    </TouchableOpacity>
                </View>


            </View>
        );
    }
}
