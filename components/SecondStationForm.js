import React, { useState } from 'react';
import { Text, View, StyleSheet, TextInput, TouchableOpacity } from 'react-native';
import Topbar from "./Topbar";
import Icon from 'react-native-vector-icons/MaterialIcons';
import { SecondStationFormCss } from '../style/SecondStationFormCss';




export default class SecondStationForm extends React.Component { 

    state={
        selected:false,
    };

    selectCheckBox=()=>{
        this.setState({
            selected:!this.state.selected
        })
    }
    
render(){

    const { selected } = this.state;

    return (
        <View>
                {/* Affichage de composant (TopBar) que déja crée aprés leur importation dans la partie import */}

            <Topbar />   
            <View style={SecondStationFormCss.container}> 

                <Text style={SecondStationFormCss.subTitle}>Address and availability</Text>
                <Text style={SecondStationFormCss.item}>Where is the station ?</Text>



                <View style={SecondStationFormCss.addressBorder}>
                    <View style={SecondStationFormCss.circleStyle}>
                        <Icon name="place" size={30} style={SecondStationFormCss.iconStyle} />
                    </View>
                    <Text style={SecondStationFormCss.addressText}>2 Rue Louis Bleriot</Text>
                    <Text style={SecondStationFormCss.secondAddressText}>Les Mureaux</Text>



                </View>
                <Text style={SecondStationFormCss.item}>When is the station is open ?</Text>
                <View style={SecondStationFormCss.timeBorder}>
                    <View style={SecondStationFormCss.viewStyle}>
                        <Text style={SecondStationFormCss.timeAvailable}>Open 24/7</Text>
                

                    </View>
                </View>


                <TouchableOpacity
                    style={SecondStationFormCss.button}
                    onPress={() => this.handlePress()}>
                    <Text style={SecondStationFormCss.text}>Next</Text>
                </TouchableOpacity>
                
            </View>
        </View>
    );
}

}


