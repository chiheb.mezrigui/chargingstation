import React from "react";
import {Text, View,StyleSheet } from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign';


export default class Topbar extends React.Component{
    render(){
        return(
            <View style={styles.container}>
            <Icon name="arrowleft" size={30} style={styles.iconStyle} />
            </View>
        );
    }
}

const styles=StyleSheet.create({

    container:{
        paddingLeft:20,
        marginBottom:25
    },

    iconStyle:{
        color:'#235ef5',
        
    }
})