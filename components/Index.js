import React, { Component } from "react";
import { View, Text, StyleSheet } from "react-native";
import { Picker } from "@react-native-picker/picker";


export default class CategoryScreen extends Component {
   
    state = {
      selectedcat: "",
      category: [
        {
            itemName: "Select phone"
          },
        {
            itemName:""
        },
       
        {
          itemName: "Nokia"
        },
        {
          itemName: "Apple"
        },
        {
          itemName: "Samsung M23"
        },
        {
          itemName: "Samsung M24"
        },
        {
          itemName: "Samsung M25"
        }
      ]
    };
  
    async onValueChangeCat(value) {
      this.setState({ selectedcat: value });
    }

    
  
    render() {
      return (
        <View style={styles.viewStyle}>
          
          <View style={{ flex: 0.7, fontSize: 14 }}>
            <Picker
              itemStyle={styles.itemStyle}
              mode="dropdown"
              style={styles.pickerStyle}
              onValueChange={this.onValueChangeCat.bind(this)}
            >
              {this.state.category.map((item) => (
                <Picker.Item
                  color="#0087F0"
                  label={item.itemName}
                  value={item.itemName}
                />
              ))}
            </Picker>
          </View>
          <View style={{ flex: 0.2 }}>
            <Text style={styles.textStyle}>{this.state.selectedcat}</Text>
          </View>
        </View>
      );
    }
  }
  
  const styles = StyleSheet.create({
    viewStyle: {
      flex: 1,
      alignSelf: "center",
      flexDirection: "row",
      width: "90%",
      alignItems: "center"
    },
    itemStyle: {
      fontSize: 10,
      color: "#007aff"
    },
    pickerStyle: {
      width: "100%",
      height: 40,
      color: "#007aff",
      fontSize: 14,
  
    },
    textStyle: {
      fontSize: 14,
    }
  });
  